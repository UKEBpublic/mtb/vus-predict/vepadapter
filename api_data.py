# DATA TO BE SERVED WITH API
import sys, os
import json

vep_results_json_file = "data/vep_results.json"

class CacheVEPResults(object):
	__instance = None
	vep_results = {}
	try:
		vep_result_file = open(vep_results_json_file, "r")
		vep_results = json.load(vep_result_file)
		vep_result_file.close()
	except IOError:
		sys.exit(os.EX_OSFILE)

	def __init__(self):
		CacheVEPResults.vep_results = self

	@staticmethod
	def getVEPResultsDict():
		if CacheVEPResults.vep_results == {}:
			CacheVEPResults()
		return CacheVEPResults.vep_results
