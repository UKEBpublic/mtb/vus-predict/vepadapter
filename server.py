from flask import Flask
from flask import request
from flask_cors import CORS
import requests
import json
import sys, os
from api_data import CacheVEPResults
from flask_swagger_ui import get_swaggerui_blueprint

# Create the application instance
app = Flask(__name__)
CORS(app)

### Swagger config ###
SWAGGER_URL = '/SIFT/v1/doc'
API_URL = '/static/openapi.json'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "VEP-API"
    }
)
app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)

import file_loader
template = file_loader.load_template('template/template.json')

@app.route('/SIFT/v1/info') 
def getMetadata():
    headers = {'PRIVATE-TOKEN': 'YqQhg-yv_3vG7t9ubDf3'}
    metadata = requests.get('http://gitlab.gwdg.de/api/v4/projects/12148/repository/branches/master', headers=headers).json()
    meta_out = {"title": 'VEP Adapter'}
    meta_out["newest_commit_id"] = metadata["commit"]["short_id"]
    meta_out["newest_commit_message"] = metadata["commit"]["title"]
    meta_out["newest_commit_date"] = metadata["commit"]["authored_date"].split("T")[0]
    meta_out["data"] = {'ensembl_vep': {'version': 'latest'}}
    meta_out["current_commit_id"], meta_out["current_commit_date"] = extractCommitData()
    return meta_out

def extractCommitData():
    with open("/app/commit_data/current_commit.json", "r") as cf:
        curl_output = cf.readlines()
        commit_info = json.loads(curl_output[0])
        return commit_info["commit"]["short_id"], commit_info["commit"]["authored_date"].split("T")[0]

@app.route('/SIFT/v1/toVEP')
# usage: '/toVEP?genompos=chr7:g.140753336A>T'
def requestSIFT():
	genom_pos = request.args.get('genompos')

	# create dictionary to store results
	result_dict = {}

	# create dictionary for SIFT predictions
	sift_prediction_dict = {}

	# fill dictionary vep_results_cached with cached results from file vep_results.json
	vep_results_cached = CacheVEPResults.getVEPResultsDict()

	if vep_results_cached != {} and genom_pos in vep_results_cached.keys():

		# extract necessary prediction information from SIFT from vep_cached_results dict
		sift_prediction_dict["Prediction"] = vep_results_cached[genom_pos]["Prediction"]
		sift_prediction_dict["Score"] = vep_results_cached[genom_pos]["Score"]
		sift_prediction_dict["Warning"] = ""

		# put sift_prediction_dict into results dict
		result_dict[genom_pos] = sift_prediction_dict

	else:

		# adjust genomic position input to request VEP: 7:g.140753336A>T
		genom_pos_processed = genom_pos.replace("chr", "")

		# request Ensembl REST API
		# information: http://rest.ensembl.org/documentation/info/vep_hgvs_get
		request_url = "http://rest.ensembl.org/vep/human/hgvs/" + genom_pos_processed + "?"

		# send request to Ensembl REST API and get result in json format
		request_result = requests.get(request_url, headers={"Content-Type": "application/json"})

		if not request_result.ok:
			result_dict[genom_pos] = {"Warning": "NOT FOUND IN VEP"}
			return result_dict

		# put output into dictionary
		request_result_dict = request_result.json()

		# get list with prediction scores
		result_prediction_list = request_result_dict[0]["transcript_consequences"]

		# initialize variables for score and prediction string over all available transcripts
		score_sum = 0
		number_entries = 0
		prediction_list = []
		prediction = ""

		# loop through list to collect all predictions and scores
		for i in range(len(result_prediction_list)):
			act_prediction_dict = result_prediction_list[i]
			if "sift_prediction" in act_prediction_dict.keys():
				prediction_list.append(act_prediction_dict["sift_prediction"])
				score_sum += act_prediction_dict["sift_score"]
				number_entries += 1

		# calculate score as majority decision
		score = score_sum / number_entries

		# count number of deleterious predictions
		pos_prediction = prediction_list.count("deleterious")
		pos_prediction += prediction_list.count("deleterious_low_confidence")

		# count number of negative predictions
		neg_prediction = prediction_list.count("tolerated")

		if pos_prediction >= neg_prediction:
			prediction = "deleterious"
		else:
			prediction = "tolerated"

		# put information into SIFT prediction dict
		sift_prediction_dict["Prediction"] = prediction
		sift_prediction_dict["Score"] = score
		sift_prediction_dict["Warning"] = ""

		# put sift_prediction_dict into results dict
		result_dict[genom_pos] = sift_prediction_dict

		# vep_results_cached dict lists entries according to ensembl transcript ID
		vep_results_cached[genom_pos] = result_dict[genom_pos]

		try:
			# write dictionary to vep_results.json file
			vep_results_json_file = "data/vep_results.json"
			json_file = open(vep_results_json_file, "w")
			json.dump(vep_results_cached, json_file)
			json_file.close()
		except IOError:
			sys.exit(os.EX_OSFILE)

		# write classification results into table for statistical analysis
		# this is just copied out of vep_results.json
		try:
			# write dictionary to vep_results.json file
			classification_sift_file = "data/classification_sift.txt"
			sift_file = open(classification_sift_file, "a")
			sift_file.write(genom_pos + "\t" + str(pos_prediction) + "\t" + str(neg_prediction) + "\n")
			sift_file.close()
		except IOError:
			sys.exit(os.EX_OSFILE)

	return result_dict

# If we're running in stand alone mode, run the application
if __name__ == '__main__':
	app.run(host='0.0.0.0', port='9006')
