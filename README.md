# Adapter for EnsEMBL VEP

## Description

Flask application that requests EnsEMBL REST API for EnsEMBL VEP and can be requested
via localhost.

Application also runs within Docker on port 5002. To run this application with Docker,
type in the following commands in your command line:

To create the image vep_flask (in the current directory):

docker image build -t vep_flask .

To run the application with the created image:

docker run -p 5002:5002 --name=vep_adapter --rm vep_flask

To choose another port to run this application, just change the docker command as follows:

docker run -p [in here goes your desired port]:5002 --name=vep_adapter --rm vep_flask

Usage: localhost:5002/toVEP?ensembl_pro=[EnsEMBL Protein ID]&ensembl_trs=[EnsEMBL
Transcript ID]&variant=[protein description of variant]

Example input: http://localhost:5002/toVEP?ensembl_pro=ENSP00000391478&ensembl_trs=ENST00000445888&variant=R175H

Example output: {"R175H":
                    {"Prediction":"tolerated",
                    "Score":"0.11",
                    "Warning":""
                }
